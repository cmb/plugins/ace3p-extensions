# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# To build locally:
# > build_sphinx <path_to_this_directory>  <path_to_output_directory>

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

source_dir = os.path.abspath('.')
sys.path.insert(0, source_dir)


# -- Project information -----------------------------------------------------

project = 'ModelBuilder for ACE3P'
copyright = '2021, Kitware'
author = 'Kitware'

# The full version, including alpha/beta/rc tags
release = '1.0.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autosectionlabel'
]

# Add any paths that contain templates here, relative to this directory.
# templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# Read RTD env vars
is_readthedocs_build = os.environ.get('READTHEDOCS', False)

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
try:
    import sphinx_rtd_theme
    html_theme = 'sphinx_rtd_theme'
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

    # Set background color to "SLAC red"
    html_theme_options = {
        'style_nav_header_background': '#8c1515'
    }
except:
    if readTheDocs:
        html_theme = 'default'
    else:
        html_theme = 'haiku'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# Custom css files
html_css_files = [
    'css/getting-started.css'
]

# Configure git-lfs for RTD environment
# Copied from https://test-builds.readthedocs.io/en/git-lfs/conf.html:
if is_readthedocs_build and not os.path.exists('./git-lfs'):
    os.system('wget https://github.com/git-lfs/git-lfs/releases/download/v2.13.3/git-lfs-linux-amd64-v2.13.3.tar.gz')
    os.system('tar xvfz git-lfs-linux-amd64-v2.13.3.tar.gz')
    os.system('./git-lfs install')  # make lfs available in current repository
    os.system('./git-lfs fetch')  # download content from remote
    os.system('./git-lfs checkout')  # make local files to have the real content on them
