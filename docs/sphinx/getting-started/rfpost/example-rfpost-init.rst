Specify RF-Postprocessing & Submit Analysis
============================================

.. rst-class:: step-number

1\. Select Rf postprocessing

First, navigate to the :guilabel:`Attribute Editor` panel. Under the :guilabel:`Modules` tab, select :guilabel:`ACDTool` from the dropdown list. A second dropdown list labeled :guilabel:`ACDTool` will appear below that. Select ``RF Postprocess`` in this second dropdown list.

.. image:: ../images/rfPost-ModulesTab.png
    :align: center
    :width: 75 %

|

When that is set, an :guilabel:`RF Postprocess` tab will be displayed in the attribute editor. (The tab will include a red "alert" icon next to its label, indicating that the contents of tab are incomplete or contains errors.) Select the :guilabel:`RF Postprocess` tab to begin specifying the postprocessing options next.


.. To begin setting up an :program:`rf-postprocessing` analysis, we need to reference a previously run :program:`Omega3P` simulation job. To do this, go to the :guilabel:`Simulation Job` panel where previously submitted simulation jobs are listed. Click on the :program:`Omega3P` job in the list which you would like to perform postprocessing on. A details panel will open at the bottom of the panel below the jobs list. In the details panel, press the clipboard button next to the :guilabel:`Remote Job Directory` line toward the bottom of the detail panel. This will copy the remote path of the :program:`Omega3P` job to the clipboard.

.. .. image:: ../images/rfPost-jobPanelCopyHighlight.png
    :align: center
    :width: 75 %

.. With that selection made in the dropdown menus, a field will appear labeled :guilabel:`NERSC Data Directory`. Paste the previously copied directory path here with :kbd:`Command-v` on macOS or :kbd:`Control-v` on Windows or by right clicking and selecting :guilabel:`Paste`.


.. rst-class:: step-number

2\. Select Omega3P Job to Postprocess

In the :guilabel:`RF-Postprocess` tab, the first step is to select the :program:`Omega3P` job results to process. This is set in the :guilabel:`ResultDir` field. For this example, you *could* simple type the text ``omega3p_results`` in the field, but instead click the tool button to the left of the text field (arrow icon).

.. image:: ../images/rfPost-ResultDir.png
    :align: center
    :width: 75 %

|

This will open a popup window listing the jobs that have been run. Select the one job listed in the popup and click the :guilabel:`Select` button.

.. image:: ../images/rfPost-SelectJob.png
    :align: center
    :width: 75 %

|

Modelbuilder will close the popup and copy the results folder name to the :guilabel:`ResultDir` field.


.. rst-class:: step-number

3\. Specify RFField Section

Below the :guilabel:`ResultDir` field is section labeled :guilabel:`RFField` with a set of data fields.

.. With the results folder to the :program:`Omega3P` job entered, we can move on to the :guilabel:`RF-Postprocess` tab. Under this tab is a menu of many parameters and options for specifying a :program:`rf-postprocessing` analysis.

.. Below the :guilabel:`ResultDir` field is a box of options labeled :guilabel:`RFField`.

.. image:: ../images/rfPost-rfFieldMenu.png
    :align: center
    :width: 75 %

|

For this example, make the following changes in the :guilabel:`RFField` section:

* :guilabel:`xsymmetry`: magnetic
* :guilabel:`ysymmetry`: magnetic
* :guilabel:`gradient`: -1
* :guilabel:`gz1`: -0.057
* :guilabel:`gz2`: 0.057
* :guilabel:`npoint`: 300
* :guilabel:`fmnx`: 10
* :guilabel:`fmny`: 10
* :guilabel:`fmnz`: 50


.. rst-class:: step-number

4\. Specify Postprocessing Options

Below the :guilabel:`RFField` section is a collection of optional analyses that are available. Clicking on the checkbox next to any of the options reveals additional options specific to that analysis. A detailed description of each of the various sub-analyses is available `here <https://www.slac.stanford.edu/%7Echo/cw18/commands/acdtool-commands.pdf>`_.

Under the optional analyses, click the :guilabel:`RoverQ` and :guilabel:`maxFieldsOnSurface` options and keep their default values.

.. image:: ../images/rfPost-OptionalAnalyses.png
    :align: center
    :width: 75 %

|

.. rst-class:: step-number

5\. Submit Postprocessing Job

Submit the :program:`ACDTool` rf-postprocessing job using the same steps described previously (starting with opening the :guilabel:`ACE3P` menu and selecting the :guilabel:`Submit Analysis Job...` item). You can review the steps at :ref:`Running Omega3P on NERSC`. With the job submitted, you can monitor its progress from the :guilabel:`Simulation Jobs panel` (see :ref:`Tracking Job Status`).
