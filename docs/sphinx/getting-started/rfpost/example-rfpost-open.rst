Open ACE3P Project
==================

If you are revisiting a previously closed project, you can open a saved project from the :program:`ACE3P` menu. Pressing :guilabel:`Open Project` will present a file browser where you may select a folder containing a project.

.. image:: ../images/rfPost-OpenProject.png
    :align: center
    :width: 50 %

|

If you closed the :program:`Omega3P` project from the previous example, use the menu to reopen it in modelbuilder.
