Stage 2: Track3P
================

Next we will add a second stage to your project for computing a Track3P simulation using the results of the S3P analysis. After creating the second stage, you will specify the Track3P simulation, submit the analysis job to NERSC, and then view the results.

.. toctree::
    :maxdepth: 1

    example-window-addstage.rst
    example-window-specify2.rst
    example-window-specify2b.rst
    example-window-submit2.rst
    example-window-view2.rst
