Other Notes and Tips
=====================

Cache and memory settings
-------------------------

On the Cori system an NERSC, :program:`ParaView` server runs on standard compute nodes and does not provide the rendering throughput of graphics (GPU) nodes. To improve the user experience visualizing time-series and large data sets in general, we recommend two changes to your default settings. You can open the :guilabel:`Settings` panel from the :guilabel:`Edit => Settings...` menu on Windows and Linux systems. On macOS systems, use the :guilabel:`modelbuilder => Preferences...` menu. With the :guilabel:`Settings` panel open:

* You should significantly increase the memory allocated to remote rendering, so that the :program:`ParaView` server will send geometry instead of imagery, because geometry provides better interactive performance. In the :guilabel:`Render View` tab, find the :guilabel:`Remote Render Threshold` item and increase the default memory threshold (megabytes) for rendering geometry. We recommend using at least a quarter of your available system memory for this, which might be 1000 MB for small systems and 4000-8000 MB on more typical present-day systems.
* You should also enable geometry caching if you are visualizing time-series data such as Track3P particles. To do this, go to the :guilabel:`General` tab, display advanced properties by clicking the gear-shaped icon near the top righthand corner, then find the :guilabel:`Cache Geometry for Animation` item and check the box next to it. (You can type "cache" in the tab's search bar to find it.)

With large animation sequences, you might experince slow rendering thoughput (slower than one update per second), but if your memory is sufficient to cache the render geometry, you can then replay animations at more interactive speeds.


ParaView Server configuration file
-----------------------------------

You might be aware that NERSC provides a configuration script for remote ParaView visualization. You should **not** load this configuration script into :program:`ModelBuilder for ACE3P`. A configuration script specific to :program:`ModelBuider for ACE3P` is included in all distributions, and is loaded automatically when remote visualization is first invoked.
