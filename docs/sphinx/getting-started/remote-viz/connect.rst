Connecting to ParaView
=======================

.. rst-class:: step-number

5\. Sign In to the SSH terminal

When you click the :guilabel:`OK` button in the :guilabel:`Connection Options` dialog, modelbuilder opens a terminal for logging into NERSC. Sign in by entering your user-password followed by a one-time-password (OTP). This creates an ssh tunnel to the remote system and submits a job to the queuing system to launch a :program:`ParaView` server.

.. image:: ../images/remote-ssh.png

|

After you sign in, a script is executed to submit the job request for ParaView server.

.. image:: ../images/remote-ssh-queued.png

|

.. important::

  The SSH window needs to stay open while using ParaView for remote visualization.


.. rst-class:: step-number

6\. Load the SLACTools Plugins

 This job request might be executed immmedately or might remain queued for some time. Once execution begins, software on the server (NERSC) will connect back to modelbuilder. When this happens, modelbuilder displays the :guilabel:`Plugin Manager` dialog with two columns for remote plugins and local plugins, respectively. Use the :guilabel:`Plugin Manager` to select and load the :guilabel:`SLACTools` plugin in both local columns. (A :guilabel:`Wating for Server Connection` dialog is displayed on top of the :guilabel:`Plugin Manager`, however, it does not prevent selecting and loading the SLACTools plugins.) After the :guilabel:`SLACTools` plugins are loaded, close the :guilabel:`Plugin Manager`.

.. image:: ../images/remote-plugin-manager.png


.. rst-class:: step-number

7\. Select Mode Files

When the :guilabel:`Plugin Manager` is closed, modelbuilder will display a :guilabel:`Mode Select` dialog (the same one used for local visualization).

.. image:: ../images/remote-mode-files.png
    :align: center

Select any of the files in the list and click the dialog :guilabel:`OK` button. ModelBuilder will then connect to the ParaView server at NERSC to construct and execute the visualization pipeline and begin delivering geometry or imagery to modelbuilder.


.. rst-class:: step-number

8\. Display the Data

When visualization data are received from the NERSC ParaView server, they are displayed in a second tab that modelbuilder adds to the rendering area. You can explore the data using the ParaView and SLACTools features described in the Omega3P example. By default, :program:`ModelBuilder for ACE3P` hides the :program:`ParaView` pipeline inspector and related toolbars and filters. To see them, find and click the |paraview-gray| (gray ParaView) toolbar button. When clicked, the button will change to the familiar red-green-blue :program:`ParaView` logo, the :guilabel:`Pipeline Browser` will be displayed in the sidebar, and several additional :program:`ParaView` toolbars will be displayed. In the :guilabel:`View` menu, you can also enable the :guilabel:`Properties` view to adjust a broad range of display details.

.. |paraview-gray| image:: ../../_static/images/postprocessing-mode_off_32.png
  :width: 24

.. image:: ../images/remote-ready-alt.png
