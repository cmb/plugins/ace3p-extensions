Creating an ACE3P Project
=========================

.. figure:: images/ace3p-menu.png
    :figwidth: 40%
    :align: right

Most of your menu interactions will be with the :guilabel:`ACE3P` project menu, which has items to open existing ModelBuilder projects, create new projects, save projects, and close projects. It also includes menu items "Add Stage..." for adding multiple analysis stages to projects, "Submit Analysis Job..." to generate :program:`ACE3P` input files and run jobs on NERSC computing resources, and "Login to NERSC" for authentication.

To provide a working example, we will be using the ``pillbox-rtop4.gen`` mesh file from the :program:`ACE3P` code workshops. You can download a copy of this file from the `ModelBuilder for ACE3P Data Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aef5cf2fa25629b9b69632>`_ if needed.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Specify a New Project Directory

To begin, open the :guilabel:`ACE3P` menu and select the :guilabel:`New Project...` item. In response, ModelBuilder displays a dialog for setting the project name and file system location for storing persistent data.

.. image:: images/new-project-name.png
    :align: center

|

.. rst-class:: step-number

2\. Input the Analysis Mesh

When you click the :guilabel:`Next` button, modelbuilder displays a dialog to select the mesh geometry file from the local file system. You can enter a Genesis (.gen) file or NetCDF (.ncdf) file. Use the :guilabel:`Browse` button to locate and enter the ``pillbox-rtop4.gen`` file and click the :guilabel:`Create Project` button.

.. image:: images/new-project-meshfile.png
    :align: center

:program:`ModelBuilder for ACE3P` will then create the new project and import the geometry file. When done, the dialog will display a :guilabel:`Finished` message. When you click the dialog's :guilabel:`Close` button, modelbuilder will display the pillbox mesh in the 3-D view and the :guilabel:`Attribute Editor` tab will contain a child tab labeled `Modules`.

.. image:: images/project-created.png
    :align: center

|
