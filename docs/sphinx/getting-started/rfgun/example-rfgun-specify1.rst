Stage 1: Omega3P
=================

The first stage uses Omega3P to compute the eigenmode frequency and EM fields for the cavity mesh.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Modules Tab

In the :guilabel:`Modules` tab, set the :guilabel:`Analysis` to ``Omega3P``.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

2\. Boundary Conditions Tab

In the :guilabel:`Boundary Conditions` tab, set side sets 1 and 5 to ``Electric``, side sets 2 through 4 to ``Magnetic``, and side set 6 to ``Exterior``.

.. image:: ../images/rfgun-boundary-conditions.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

3\. Analysis Tab

In the :guilabel:`Analysis` tab, change :guilabel:`Frequency Shift (Hz)` to ``2.8e9``.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

4\. Submit Job

If you have not logged in to NERSC from modelbuilder, go to the :guilabel:`ACE3P` menu and select the :guilabel:`Login to NERSC` item to bring up the login dialog. Complete the items in the dialog and click :guilabel:`OK` to log in.

Then go to the :guilabel:`ACE3P` menu and select the :guilabel:`Submit Analysis Job…` item to bring up the job-submit dialog. Enter your info in the :guilabel:`Project Repository` field and click the :guilabel:`Apply` button to submit the job.

.. rst-class:: step-number

5\. Download Results

This step is optional, but once the job has completed, you can use the tool buttons in the :guilabel:`Simulation Jobs` panel to download the results and view the EM field data. When modelbuilder opens the :guilabel:`Mode Files` dialog, select the mode file and click the :guilabel:`OK` button.

.. image:: ../images/rfgun-result1.png
    :align: center

|
