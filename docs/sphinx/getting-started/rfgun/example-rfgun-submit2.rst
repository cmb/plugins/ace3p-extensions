Run TEM3P Analysis
===================

.. rst-class:: step-number

1\. Submit Job

Make sure you are logged into NERSC, then open the :guilabel:`ACE3P` menu and select the :guilabel:`Submit Analysis Job...` item to open the job-submit dialog. Enter your :guilabel:`Project repository`, then scroll down to the :guilabel:`Number of cores` field and enter ``32``. You can use the default values for the rest of the fields in the dialog.

.. important::
    Given the size of this simulation, be sure to change the :guilabel:`Number of cores` from ``1`` to ``32`` so that the job will run in a reasonable time.

Once you submit the job, :program:`ModelBuilder for ACE3P` will add a row for it in the :guilabel:`Simulation Jobs` panel and begin polling NERSC for status updates.


.. rst-class:: step-number

2\. Download Job Output Data

After the job has completed, download the results using the "Download" tool button in the :guilabel:`Simulation Jobs` panel, as described in :ref:`Downloading Omega3P Results`.


.. rst-class:: step-number

3\. Display the Results Data

When the download is finished, you can view the computed field data by clicking the "Display" tool button as described in :ref:`Viewing Omega3P Results`. When modelbuilder opens the :guilabel:`Mode Files` dialog, select ``Displacement.mod`` and click the :guilabel:`OK` button. You can also enable the :program:`ParaView` features to explore all of the fields computed by this analysis in more detail.

.. image:: ../images/rfgun-result2.png
    :align: center

|
