Running Omega3P on NERSC
========================

You can submit jobs directly to the computing resources at NERSC from the :program:`ModelBuilder for ACE3P` user interface. There is no need to open a terminal and use ``ssh`` or ``scp``.

.. rst-class:: step-number

7\. Login to NERSC

To submit jobs, first connect to a NERSC login node from :program:`ModelBuilder for ACE3P`. To do this, go to the :guilabel:`ACE3P` menu and select the :guilabel:`Login to NERSC` item. :program:`ModelBuilder` will display a dialog for entering your NERSC user name, password and Multi-Factor Authentication (MFA) string. Note that your credentials are passed directly to NERSC and are not stored in ModelBuilder. After you click the "OK" button, ModelBuilder will connect to NERSC, passing your credentials, and display a confirmation dialog.

.. image:: images/nersc-login.png
    :align: center

|

.. rst-class:: step-number

8\. Open the Submit Analysis Dialog

In the :guilabel:`ACE3P` menu, select the :guilabel:`Submit Analysis Job...` item to open a dialog for generating the :program:`Omega3P` input file. Find the checkbox labeled :guilabel:`Submit job to NERSC` and click on it to display additional fields for submitting the job. In this example, you only need to enter one field, the NERSC project repository (if you don't know your project repository, consult with your project PI), and use the default values for the other fields. (Be sure to scroll down and take note of what fields are available, including the number of nodes and cores to use.) After entering the repository field, click the :guilabel:`Apply` button to submit the job. It usually takes 5-10 seconds for the files to be uploaded to NERSC and a new job created. When the dialog closes, you can check the :guilabel:`Simulation Jobs` view to track the job's progress.

.. image:: images/project-export.png
    :align: center

|

.. note:: If you run multiple different :program:`Omega3P` jobs in the same project, be sure to edit the :guilabel:`Results directory` field each time so that each job writes to a different directory.
