Installing ModelBuilder for ACE3P
=================================

The latest release packages for :program:`ModelBuilder for ACE3P` are stored online in the `ModelBuilder for ACE3P Downloads Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aec5b32fa25629b9b6798b>`_. Navigate a web browser to that link, locate the package file for your platform, and click the download button to the right of it.

.. image:: images/download-page.png

After the download is complete:

* For linux, extract the files from the :file:`.tar.gz` package to a convenient folder on your system. The modelbuilder executable is in the ``bin`` folder.
* For macOS, open the ``.dmg`` package and drag the :program:`modelbuilder.app` icon to a folder on your system. Because we don't distribute :program:`ModelBuilder for ACE3P` through Apple's App Store, macOS will display warning dialogs when you unpack the ``.dmg`` file and start the application for the first time.
* For Windows, unzip the ``.zip`` package to a convenient folder. The modelbuilder executable is in the ``bin`` folder.

When you first start :program:`ModelBuilder for ACE3P`, it looks similar to the :program:`ParaView` desktop application, with the main differences being that (i) some of the :program:`ParaView` toolbars have been hidden, and (ii) the left sidebar has two new dock widgets labeled :guilabel:`Resources` and :guilabel:`Attribute Editor`, and (iii) there is a new :guilabel:`ACE3P` project menu.

.. image:: images/modelbuilder-start.png
    :align: center

|
