View Track3P Results
====================

.. note::
    We will describe the steps to download and view the :program:`Track3P` results, but note that this download will take much longer than previous examples because it includes 200 particle files. You may instead skip this page  and display the results remotely as described in :ref:`Example: Remote Visualization`.


.. rst-class:: step-number

1\. Download the Results Data

Once the job has completed, download the results using the "Download" tool button in the :guilabel:`Simulation Jobs` panel, as described in :ref:`Downloading Omega3P Results`.

.. image:: ./images/download-toolbutton-highlight.png
    :align: center

|

.. rst-class:: step-number

2\. Display the Results Data

When the download is finished, you can view the EM field and computed particles data by clicking the "Display" tool button as described in :ref:`Viewing Omega3P Results`.

.. image:: ./images/render-toolbutton-highlight.png
    :align: center

|

.. rst-class:: step-number

2\. View the Particles Animation

Carry out these steps:

a. If the SLACTools plugin is not loaded, go to the :guilabel:`Tools` menu and select :guilabel:`Manage Plugins...` to open the :guilabel:`Plugin Manager` and load SLACTools.
b. In the SLACTools toolbar, find and press the button with cylindrical wireframe that has the tooltip ``Show Wireframe Front and Solid Back``.

.. image:: ./images/slactools-button.png
    :align: center

|

c. Find and click the gray :program:`ParaView` button to add a number of toolbars to the user interface. (Once clicked, the button will also change to display the familiar red-green-blue colors.)

.. image:: ./images/paraview-toolbutton.png
    :align: center

|

d. Find the VCR style controls and click the "Play" button to playback the simulation steps.

.. image:: ./images/track3p-results-play-annotated.png
    :align: center

|
