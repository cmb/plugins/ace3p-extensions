Example: Multiple Stages (Window)
=================================

.. image:: ./images/window-result2.png

In this example, you will go through the steps to create a multi-stage :program:`ACE3P` project to carry out a multipacting simulation for a TTFIII window coupler. The first stage of the project will compute EM field data using S3P and the second stage will launch and track particle trajectories using Track3P. In this tutorial you will create a new project, specify the analysis parameters for the first stage (S3P), submit the analysis job to NERSC, and then view the results.  Following that, you will add a second stage to the project and run through the same three steps (specify analysis, submit job, view results) for Track3P simulation. Before beginning this tutorial, users should first complete the :ref:`Omega3P Analysis <Example: Omega3P Analysis>` example, using :program:`Modelbuilder for ACE3P` to create an ACE3P project, edit simulation parameters, submit analysis jobs to NERSC, and view analysis results.

.. toctree::
    :maxdepth: 1

    example-window-create.rst
    example-window-stage1.rst
    example-window-stage2.rst

.. note::
    Screenshots in this section were taken from Modelbuilder running on Windows 11.
