Example: Omega3P Analysis
=========================

.. image:: images/results-view.png

In this example, we'll go through the steps to create an :program:`ACE3P` project, specify an Omega3P problem, submit an Omega3P simulation job on the Cori machine at NERSC, track the job progress, download the job results, and visualize the computed EM fields. Readers should have some familiarity with :program:`ACE3P` and :program:`ParaView` and, in particular, the SLACTools plugin for displaying :program:`ACE3P` results data in :program:`ParaView`. There is also a slightly out-of-date video `Running Omega3P and ACDTool <https://vimeo.com/563481838/2bb13e6c1c/>`_ available for viewing online that covers the steps in this example.


.. toctree::
    :maxdepth: 1

    example-omega3p-create.rst
    example-omega3p-specify.rst
    example-omega3p-submit.rst
    example-omega3p-track.rst
    example-omega3p-download.rst
    example-omega3p-view.rst
    example-omega3p-close.rst

.. note::
    Screenshots in this section were taken from Modelbuilder running on Ubuntu.
