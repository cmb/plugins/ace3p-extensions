//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtNewtLoginDialog.h"
#include "ui_qtNewtLoginDialog.h"

#include <QDialogButtonBox>
#include <QLineEdit>

namespace newt
{

qtNewtLoginDialog::qtNewtLoginDialog(QWidget* parentObject)
  : QDialog(parentObject)
  , ui(new Ui::qtNewtLoginDialog)
{
  ui->setupUi(this);

  connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &qtNewtLoginDialog::canceled);
}

qtNewtLoginDialog::~qtNewtLoginDialog()
{
  delete ui;
}

void qtNewtLoginDialog::accept()
{
  QString password = ui->passwordEdit->text() + ui->mfaEdit->text();
  Q_EMIT entered(ui->usernameEdit->text(), password);
  ui->passwordEdit->clear();
  ui->mfaEdit->clear();
  ui->messageLabel->clear();
  QDialog::accept();
}

void qtNewtLoginDialog::reject()
{
  ui->passwordEdit->clear();
  ui->mfaEdit->clear();
  ui->messageLabel->clear();
  QDialog::reject();
}

void qtNewtLoginDialog::setErrorMessage(const QString& message)
{
  ui->messageLabel->setText(message);
}

} // namespace newt
