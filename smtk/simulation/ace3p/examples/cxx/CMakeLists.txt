set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

# Add cumuusJobTracker executable
# add_executable(cumulusJobTracker
#   jobTracker.cxx
#   qtJobTrackerTestWidget.cxx
#   qtJobTrackerTestWidget.h
#   qtJobTrackerTestWidget.ui
# )
# target_link_libraries(cumulusJobTracker
#   smtkACE3PQtExt
#   smtkCumulus
#   smtkNewt
# )

# Add newProjectWizard executable
add_executable(newProjectWizard
  newProjectWizard.cxx
)
target_compile_definitions(newProjectWizard PRIVATE QT_NO_KEYWORDS)
target_compile_definitions(newProjectWizard
  PRIVATE "WORKFLOWS_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}/simulation-workflows\""
)
target_link_libraries(newProjectWizard
  smtkVTKSession
  smtkACE3PQtExt
)

# Add newtJobTracker executable
add_executable(newtJobTracker
  jobTracker.cxx
  qtJobTrackerTestWidget.cxx
  qtJobTrackerTestWidget.h
  qtJobTrackerTestWidget.ui
)
target_compile_definitions(newtJobTracker PRIVATE QT_NO_KEYWORDS)
target_compile_definitions(newtJobTracker PRIVATE USE_NEWT_TRACKER)
target_link_libraries(newtJobTracker
  smtkACE3PQtExt
  # smtkCumulus
  smtkNewt
)

# Add newtProjectSubmit executable
add_executable(newtProjectSubmit
  projectSubmit.cxx
  qtProjectSubmitTestWidget.cxx
  qtProjectSubmitTestWidget.h
  qtProjectSubmitTestWidget.ui
)
target_compile_definitions(newtProjectSubmit PRIVATE QT_NO_KEYWORDS)
target_compile_definitions(newtProjectSubmit
  PRIVATE
    USE_NEWT_INTERFACE
    "WORKFLOWS_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}/simulation-workflows\""
)
target_link_libraries(newtProjectSubmit
  smtkVTKSession
  smtkACE3PQtExt
  # smtkCumulus
  smtkNewt
)

# Add qtProgressDialogTestWidget executable
add_executable(qtProgressDialogTestWidget
  progressDialog.cxx
  qtProgressDialogTestWidget.cxx
  qtProgressDialogTestWidget.h
  qtProgressDialogTestWidget.ui
)
target_compile_definitions(qtProgressDialogTestWidget PRIVATE QT_NO_KEYWORDS)
target_link_libraries(qtProgressDialogTestWidget
  smtkACE3PQtExt
)

# Add example programs for RF gun example
set(commandline_examples
createProject
createRfGunProject)

foreach (example ${commandline_examples})
  add_executable( ${example}
    ${example}.cxx)
  target_compile_definitions(${example} PRIVATE QT_NO_KEYWORDS)
  target_compile_definitions(${example} PRIVATE "DATA_DIR=\"${PROJECT_SOURCE_DIR}/data\"")
  target_compile_definitions(${example} PRIVATE "SCRATCH_DIR=\"${CMAKE_BINARY_DIR}/Testing/Temporary\"")
  target_compile_definitions(${example} PRIVATE "WORKFLOWS_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}/simulation-workflows\"")

  target_link_libraries(${example}
    smtkVTKSession
    smtkACE3P
    smtkCore
    Boost::filesystem
  )
endforeach()
