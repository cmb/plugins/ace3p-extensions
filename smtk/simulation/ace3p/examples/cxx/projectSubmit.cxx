//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/newt/qtApplicationLoginSupport.h"
#include "smtk/simulation/ace3p/Metadata.h"
#include "smtk/simulation/ace3p/Project.h"
#include "smtk/simulation/ace3p/Registrar.h"
#include "smtk/simulation/ace3p/examples/cxx/qtProjectSubmitTestWidget.h"

// SMTK includes
#include "smtk/model/Registrar.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/project/Registrar.h"
#include "smtk/resource/Manager.h"
#include "smtk/session/vtk/Registrar.h"

#include <QApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QDebug>
#include <QString>
#include <QStringList>

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);

  newt::qtApplicationLoginSupport support;
  if (!support.checkSSL())
  {
    return -1;
  }

  // Include command-line option for backdoor mock login
  QCommandLineParser parser;
  parser.addHelpOption();

  QCommandLineOption userOption(
    QStringList() << "u"
                  << "user",
    "NERSC user",
    "user");
  parser.addOption(userOption);

  QCommandLineOption idOption(
    QStringList() << "n"
                  << "newt-sessionid",
    "NEWT session id",
    "string");
  parser.addOption(idOption);

  QCommandLineOption projectOption(
    QStringList() << "p"
                  << "project",
    "Path to ACE3P project directory",
    "directory");
  parser.addOption(projectOption);

  QCommandLineOption repositoryOption(
    QStringList() << "r"
                  << "repository",
    "NERSC repository",
    "string");
  parser.addOption(repositoryOption);

  parser.process(app);

// Application must set workflows directory
#ifndef WORKFLOWS_SOURCE_DIR
#error WORKFLOWS_SOURCE_DIR must be defined
#endif
  smtk::simulation::ace3p::Metadata::WORKFLOWS_DIRECTORY = WORKFLOWS_SOURCE_DIR;

  // Create smtk managers
  smtk::resource::ManagerPtr resManager = smtk::resource::Manager::create();
  smtk::attribute::Registrar::registerTo(resManager);
  smtk::model::Registrar::registerTo(resManager);
  smtk::session::vtk::Registrar::registerTo(resManager);

  smtk::operation::ManagerPtr opManager = smtk::operation::Manager::create();
  smtk::operation::Registrar::registerTo(opManager);
  smtk::attribute::Registrar::registerTo(opManager);
  smtk::session::vtk::Registrar::registerTo(opManager);

  smtk::project::ManagerPtr projManager = smtk::project::Manager::create(resManager, opManager);
  smtk::project::Registrar::registerTo(projManager);
  smtk::simulation::ace3p::Registrar::registerTo(projManager);

  opManager->registerResourceManager(resManager);

  // Display the widget
  auto* w = new smtk::simulation::ace3p::qtProjectSubmitTestWidget(projManager);

  // Initialize any command-line arguments
  if (parser.isSet(userOption))
  {
    QString user = parser.value(userOption);
    w->setUser(user);
  }
  if (parser.isSet(idOption))
  {
    QString newtSessionId = parser.value(idOption);
    w->setNewtSessionId(newtSessionId);
  }
  if (parser.isSet(projectOption))
  {
    QString projDir = parser.value(projectOption);
    w->setProjectDirectory(projDir);
  }
  if (parser.isSet(repositoryOption))
  {
    QString repository = parser.value(repositoryOption);
    w->setNerscRepository(repository);
  }

  w->show();

  int retval = app.exec();
  return retval;
}
