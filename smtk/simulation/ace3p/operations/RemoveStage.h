//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_project_RemoveStage_h
#define smtk_simulation_ace3p_project_RemoveStage_h

#include "smtk/simulation/ace3p/Exports.h"

#include "smtk/project/Operation.h"

namespace smtk
{
namespace simulation
{
namespace ace3p
{

/** \brief Operation to remove a stage from the project. */
class SMTKACE3P_EXPORT RemoveStage : public smtk::project::Operation
{
public:
  smtkTypeMacro(smtk::simulation::ace3p::RemoveStage);
  smtkCreateMacro(RemoveStage);
  smtkSharedFromThisMacro(smtk::operation::Operation);

protected:
  smtk::operation::Operation::Result operateInternal() override;
  smtk::operation::Operation::Specification createSpecification() override;
  virtual const char* xmlDescription() const override;
};
} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
