<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the ACE3P Project "RemoveStage" Operation -->
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <!-- Parameters -->
    <AttDef Type="newAnalysis" Label="Project - New Analysis" BaseType="operation">
      <BriefDescription>
        Remove a Stage (Attribute Resource) from the project.
      </BriefDescription>

      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       Extensible="false" OnlyResources="true"
                       AdvanceLevel="1">
        <Accepts>
          <Resource Name="smtk::simulation::ace3p::Project"/>
          <!-- Accept *any* project as temp workaround -->
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>

      <ItemDefinitions>
        <Int Name="stage-index" Label="Stage Index">
          <BriefDescription>
            The index of the stage in the project stage vector.
          </BriefDescription>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(newAnalysis)" BaseType="result">
      <ItemDefinitions>

        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::simulation::ace3p::Project"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
