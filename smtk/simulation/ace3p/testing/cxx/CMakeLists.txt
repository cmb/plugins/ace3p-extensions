set(unit_tests
TestAddJob.cxx
  TestCreateProject.cxx
  TestExportProject.cxx
  TestNewStage.cxx
  )

set(unit_tests_which_require_data)

unit_tests(
  LABEL "UnitTest"
  SOURCES ${unit_tests}
  SOURCES_REQUIRE_DATA ${unit_tests_which_require_data}
  LIBRARIES smtkCore smtkACE3P smtkACE3PQtExt smtkVTKSession Boost::filesystem
  )

foreach (test ${unit_tests})
  get_filename_component(tname ${test} NAME_WE)
  set_tests_properties(${tname}
    PROPERTIES
      ENVIRONMENT "PYTHONPATH=${smtk_module_dir}:${CMAKE_INSTALL_PREFIX}/${PYTHON_MODULEDIR}:${PROJECT_BINARY_DIR}:$ENV{PYTHONPATH}"
      SKIP_RETURN_CODE 125
    )
endforeach ()
foreach (test ${unit_tests_which_require_data})
  get_filename_component(tname ${test} NAME_WE)
  set_tests_properties(${tname}
    PROPERTIES
      ENVIRONMENT "PYTHONPATH=${smtk_module_dir}:$
      {CMAKE_INSTALL_PREFIX}/${PYTHON_MODULEDIR}:${PROJECT_BINARY_DIR}:$ENV{PYTHONPATH}"
      SKIP_RETURN_CODE 125
    )
endforeach ()

# Add python env for export test
set(smtk_module_dir ${smtk_PREFIX_PATH}/${SMTK_PYTHONPATH})
if(NOT IS_ABSOLUTE ${smtk_module_dir})
  get_filename_component(smtk_module_dir
    ${PROJECT_BINARY_DIR}/${smtk_DIR}/${SMTK_PYTHON_MODULEDIR} ABSOLUTE)
endif()

set(pyenv
  ${PROJECT_BINARY_DIR}
  ${PROJECT_BINARY_DIR}/smtksimulationace3p
  ${smtk_module_dir}
  $ENV{PYTHONPATH})
if (WIN32)
  string(REPLACE ";" "\;" pyenv "${pyenv}")
else ()
  string(REPLACE ";" ":" pyenv "${pyenv}")
endif()

set(pathenv)
if (WIN32)
  set(pathenv
    ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
    $ENV{PATH})
  string(REPLACE ";" "\;" pathenv "${pathenv}")
endif()

set_tests_properties(TestExportProject
  PROPERTIES
    ENVIRONMENT "PYTHONPATH=${pyenv}"
)
if (pathenv)
  set_property(TEST TestExportProject APPEND
    PROPERTY
      ENVIRONMENT "PATH=${pathenv}"
    )
endif ()

# Add dependency to export_project
set_tests_properties(TestExportProject
  PROPERTIES
    DEPENDS TestCreateProject
)
