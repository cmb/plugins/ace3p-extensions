//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_ace3p_Metadata_h
#define pybind_smtk_simulation_ace3p_Metadata_h

#include <pybind11/pybind11.h>

#include "smtk/simulation/ace3p/Metadata.h"

namespace py = pybind11;

inline py::class_<smtk::simulation::ace3p::Metadata> pybind11_init_smtk_simulation_ace3p_Metadata(
  py::module& m)
{
  py::class_<smtk::simulation::ace3p::Metadata> instance(m, "Metadata");
  instance.def(py::init<>())
    .def_readonly_static("PROJECT_TYPENAME", &smtk::simulation::ace3p::Metadata::PROJECT_TYPENAME)
    .def_readonly_static(
      "PROJECT_FILE_EXTENSION", &smtk::simulation::ace3p::Metadata::PROJECT_FILE_EXTENSION)
    .def_readonly_static(
      "METADATA_PROPERTY_KEY", &smtk::simulation::ace3p::Metadata::METADATA_PROPERTY_KEY)
    .def_readonly_static(
      "ANALYSIS_MESH_ROLE", &smtk::simulation::ace3p::Metadata::ANALYSIS_MESH_ROLE)
    .def_readwrite_static(
      "WORKFLOWS_DIRECTORY", &smtk::simulation::ace3p::Metadata::WORKFLOWS_DIRECTORY)
    .def("getFromResource", &smtk::simulation::ace3p::Metadata::getFromResource)
    .def("putToResource", &smtk::simulation::ace3p::Metadata::putToResource)
    .def_readonly("Format", &smtk::simulation::ace3p::Metadata::Format)
    .def_readwrite(
      "NativeAnalysisMeshLocation", &smtk::simulation::ace3p::Metadata::NativeAnalysisMeshLocation);
  return instance;
}

#endif
