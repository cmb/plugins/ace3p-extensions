
//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtNewtJobSubmitter_h
#define smtk_simulation_ace3p_qt_qtNewtJobSubmitter_h

#include <QObject>
#include <QString>

#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/newt/qtNewtInterface.h"
#include "smtk/simulation/ace3p/qt/qtTypeDeclarations.h"

#include "smtk/PublicPointerDefs.h"

#include <QObject>
#include <QString>

#include "nlohmann/json.hpp"

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class Project;

/* \brief Handles Newt I/O for submitting jobs to NERSC.
 * *
 */
class SMTKACE3PQTEXT_EXPORT qtNewtJobSubmitter : public QObject
{
  Q_OBJECT

public:
  qtNewtJobSubmitter(QObject* parent = nullptr);
  ~qtNewtJobSubmitter() override;

  void submitAnalysisJob(
    smtk::project::ProjectPtr project,
    smtk::attribute::ConstAttributePtr exportSpec,
    smtk::attribute::ConstAttributePtr exportResult);

  QString getUniqueJobName(smtk::project::ProjectPtr project, const QString& prefix);

Q_SIGNALS:
  void errorMessage(const QString& text);
  void progressMessage(const QString& text);
  void jobSubmitted(const nlohmann::json);
  void jobOverwritten(const QString& jobId);

protected Q_SLOTS:
  void onCreateJobDirectoryFinished();
  void onClearResultsDirectoryFinished();
  void onQueryMeshFileFinished(bool uploadMesh);
  void onUploadJobFilesFinished();
  void onSubmitJobFinished();

protected:
  /** \brief Creates job directory on remote machine */
  void createJobDirectory();
  /** \brief Makes sure results directory is empty */
  void clearResultsDirectory();
  /** \brief Uploads files to the remote machine */
  void uploadJobFiles();
  /** \brief Sends submit command to remote machine */
  void submitJob();
  /** \brief Queries NERSC for NCDF file */
  void queryMeshFile();

private:
  class Internal;
  Internal* m_internal;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk
#endif
