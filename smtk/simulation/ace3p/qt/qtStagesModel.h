//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtStagesModel_h
#define smtk_simulation_ace3p_qt_qtStagesModel_h

#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/simulation/ace3p/Project.h"

// smtk includes
#include "smtk/PublicPointerDefs.h"

// Qt includes
#include <QAbstractTableModel>
#include <QItemSelectionModel>

class QStringList;

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3PQTEXT_EXPORT qtStagesModel : public QAbstractTableModel
{
  Q_OBJECT
  using Superclass = QAbstractTableModel;

Q_SIGNALS:
  void selectedStage(const smtk::resource::ResourcePtr);

public:
  qtStagesModel(QWidget* parent);
  virtual ~qtStagesModel() = default;

  /** \brief Returns the number of rows in the Model/View - from base class. */
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  /** \brief Returns the number of columns in the Model/View - from base class. */
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  /** \brief Returns the column headers of the Model/View - from base class. */
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole)
    const override;
  /** \brief Returns individual elements of the Model - from base class. */
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  /** \brief Edits individual elements of the Model - from base class. */
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
  /** \brief Sets editing flags of the Model - from base class. */
  Qt::ItemFlags flags(const QModelIndex& index) const override;
  /** \brief Adds row(s) to the Model/View - from base class. */
  bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
  /** \brief Removes row(s) from the Model/View - from base class. */
  bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;

  //void insertStage(smtk::resource::ResourcePtr newStage);

  /** \brief Adds a stage to the Model/View. */
  void addStage(int stageNumber);

  /** \brief Sets `m_ace3pProject`. */
  void setProject(smtk::project::ProjectPtr project);

  int projectCurrentStage() const;

  /** \brief Removes a stage from the Model/View. */
  void removeStage(int rowIndex);

protected:
private:
  // column headers on the table
  const QStringList m_headers = { "Name", "ACE3P Code", "Description", "Analysis Mesh" };

  const std::string m_defaultDescp = "Write your description of this stage.";
  const int m_textAlignmentRoles[4] = { Qt::AlignCenter,
                                        Qt::AlignCenter,
                                        Qt::AlignVCenter | Qt::AlignJustify,
                                        Qt::AlignCenter };
  /** number of columns in the table */
  const int m_nCols = 4;

  int m_projectCurrentStage;

  std::shared_ptr<Project> m_ace3pProject;

  // number of rows in the table
  //int m_nRows = 0;

  // @brief list of stages tracked by model
  //std::vector<smtk::resource::ResourcePtr> m_stages;

  // 1) where to get actual stage, not jsut UUID? need to access stage info (like descriptions)

  // @brief placeholder text description strings
  // @note (CURRENTLY VOLITILE, TO DO: serialize into Attribute resource)
  //std::vector<std::string> m_descriptions;
};
} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
