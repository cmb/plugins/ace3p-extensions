//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtMenuSeparator.h"

#include <QFont>
#include <QFrame>
#include <QHBoxLayout>
#include <QLabel>
#include <QSize>
#include <QWidget>

namespace
{
QSize IconSize(16, 16);
}

qtMenuSeparator::qtMenuSeparator(const QString& text, const QIcon& icon, QObject* parent)
  : QWidgetAction(parent)
  , m_text(text)
  , m_icon(icon)
{
}

QWidget* qtMenuSeparator::createWidget(QWidget* parent)
{
  QWidget* separator = new QWidget(parent); // return value
  QHBoxLayout* layout = new QHBoxLayout();
  layout->setContentsMargins(4, 6, 8, 0);
  separator->setLayout(layout);

  // Use empty QFrame for line
  QFrame* leftLine = new QFrame();
  leftLine->setFrameShape(QFrame::HLine);
  leftLine->setFrameShadow(QFrame::Sunken);
  layout->addWidget(leftLine);
  layout->setStretchFactor(leftLine, 1);

  QLabel* iconLabel = new QLabel();
  iconLabel->setPixmap(m_icon.pixmap(IconSize));
  layout->addWidget(iconLabel);

  QLabel* textLabel = new QLabel(m_text);
  QFont font = textLabel->font();
  int fontSize = static_cast<int>(1.2 * font.pointSize());
  font.setPointSize(fontSize);
  textLabel->setFont(font);
  layout->addWidget(textLabel);

  QFrame* rightLine = new QFrame();
  rightLine->setFrameShape(QFrame::HLine);
  rightLine->setFrameShadow(QFrame::Sunken);
  layout->addWidget(rightLine);
  layout->setStretchFactor(rightLine, 1);

  return separator;
}
