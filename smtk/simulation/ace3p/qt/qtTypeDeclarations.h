//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtTypeDeclarations_h
#define smtk_simulation_ace3p_qt_qtTypeDeclarations_h

#include "nlohmann/json.hpp"

#include <QMetaType>

// For passing job records in signal/slot connections
Q_DECLARE_METATYPE(nlohmann::json)

#endif // smtk_simulation_ace3p_qt_qtTypeDeclarations_h
