set(ui_files
  qtJobsWidget.ui
  qtModeSelectDialog.ui
  qtNewProjectMeshPage.ui
  qtNewProjectNamePage.ui
  qtProgressDialog.ui
  qtStagesWidget.ui
)

qt5_add_resources (RCC_SOURCES icons/jobWidgetIcons.qrc)

set(qt_sources
  ${RCC_SOURCES}
  qtAbstractJobTracker.cxx
  # qtCumulusJobTracker.cxx
  qtJobsModel.cxx
  qtJobsWidget.cxx
  qtMenuSeparator.cxx
  qtMessageDialog.cxx
  qtModeSelectDialog.cxx
  qtNerscFileItem.cxx
  qtNewProjectMeshPage.cxx
  qtNewProjectNamePage.cxx
  qtNewProjectOperatePage.cxx
  qtNewProjectWizard.cxx
  qtNewtJobSubmitter.cxx
  qtNewtJobTracker.cxx
  qtOperationLauncher.cxx
  qtProgressDialog.cxx
  qtProjectRuntime.cxx
  qtStagesModel.cxx
  qtStagesWidget.cxx
  )

set(qt_headers
  qtAbstractJobTracker.h
  # qtCumulusJobTracker.h
  qtJobsModel.h
  qtJobsWidget.h
  qtMenuSeparator.h
  qtMessageDialog.h
  qtModeSelectDialog.h
  qtNerscFileItem.h
  qtNewProjectMeshPage.h
  qtNewProjectNamePage.h
  qtNewProjectOperatePage.h
  qtNewProjectWizard.h
  qtNewtJobSubmitter.h
  qtNewtJobTracker.h
  qtOperationLauncher.h
  qtProgressDialog.h
  qtProjectRuntime.h
  qtStagesModel.h
  qtStagesWidget.h
  qtTypeDeclarations.h
  )

set(install_hdrs
  ${qt_headers}
)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

add_library(smtkACE3PQtExt
  ${qt_sources}
  ${MOC_BUILT_SOURCES}
  ${ui_files}
)
target_compile_definitions(smtkACE3PQtExt PRIVATE QT_NO_KEYWORDS)
smtk_public_headers(smtkACE3PQtExt ${install_hdrs})

# Add location of moc files
target_include_directories(smtkACE3PQtExt PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
target_include_directories(smtkACE3PQtExt
  PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
    $<INSTALL_INTERFACE:include>
  # PRIVATE
  #   ${Boost_INCLUDE_DIRS}
)

target_link_libraries(smtkACE3PQtExt
  LINK_PUBLIC
    smtkACE3P
    smtkNewt
    smtkQtExt
    smtkCore
    # smtkCumulus
    Qt5::Core
    Qt5::Widgets
  )
smtk_export_header(smtkACE3PQtExt Exports.h)

smtk_install_library(smtkACE3PQtExt)
