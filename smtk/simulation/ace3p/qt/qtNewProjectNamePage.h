//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_simulation_ace3p_qt_qtNewProjectNamePage_h
#define smtk_simulation_ace3p_qt_qtNewProjectNamePage_h

#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QWizardPage>

namespace Ui
{
class qtNewProjectNamePage;
}

class QFileDialog;

/** \brief Wizard page for setting new project name and location. */

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3PQTEXT_EXPORT qtNewProjectNamePage : public QWizardPage
{
  Q_OBJECT
  using Superclass = QWizardPage;

public:
  qtNewProjectNamePage(QWidget* parent = nullptr);
  ~qtNewProjectNamePage() = default;

  /** \brief Sets UI focus on name field and selects all text. */
  void selectName();
  /** \brief Read accessor for the project location. */
  QString projectLocation() const;
  /** \brief Write accessor for the project location. */
  void setProjectLocation(const QString& path);

  /** \brief Validates UI form data prior to using/storing those data. */
  bool validatePage() override;

protected Q_SLOTS:
  void browseLocation();

protected:
  QString getUniqueProjectName(const QString& location) const;

private:
  Ui::qtNewProjectNamePage* ui;
  QFileDialog* m_fileDialog;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
