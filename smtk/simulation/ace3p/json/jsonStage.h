//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_json_jsonStage_h
#define smtk_simulation_ace3p_json_jsonStage_h

#include "smtk/simulation/ace3p/Exports.h"

#include "nlohmann/json.hpp"

// Define how project Stage are serialized.
namespace smtk
{
namespace simulation
{
namespace ace3p
{
using json = nlohmann::json;

class Project;
class Stage;

SMTKACE3P_EXPORT void to_json(json&, const std::shared_ptr<Stage>& stage);

// Omitting from_json() for better encapsulation of Stage.
// Instead use parse_json() function, which returns Stage (shared pointer).
SMTKACE3P_EXPORT std::shared_ptr<Stage> parse_json(const json&, std::shared_ptr<Project>);
} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
