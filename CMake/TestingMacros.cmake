# Declare unit tests Usage:
#
# smtk_unit_tests(
#   LABEL <prefix for all unit tests>
#   SOURCES <test_source_list>
#   SOURCES_REQUIRE_DATA <test_sources_that_require_DATA_DIR>
#   EXTRA_SOURCES <helper_source_files>
#   LIBRARIES <dependent_library_list>
#   )
function(unit_tests)
  set(options)
  set(oneValueArgs)
  set(multiValueArgs LABEL SOURCES SOURCES_REQUIRE_DATA EXTRA_SOURCES LIBRARIES)
  cmake_parse_arguments(ut
    "${options}" "${oneValueArgs}" "${multiValueArgs}"
    ${ARGN}
    )

  list(APPEND ut_SOURCES ${ut_SOURCES_REQUIRE_DATA})

  list(LENGTH ut_SOURCES num_sources)
  if(NOT ${num_sources})
    #no sources don't make a target
    return()
  endif()

  if (ENABLE_TESTING)
    smtk_get_kit_name(kit)
    #we use UnitTests_ so that it is a unique key to exclude from coverage
    set(test_prog UnitTests_${kit})

    create_test_sourcelist(TestSources ${test_prog}.cxx ${ut_SOURCES})
    add_executable(${test_prog} ${TestSources} ${ut_EXTRA_SOURCES})
    target_compile_definitions(${test_prog} PRIVATE QT_NO_KEYWORDS)

    target_link_libraries(${test_prog} LINK_PRIVATE ${ut_LIBRARIES})
    target_include_directories(${test_prog}
        PRIVATE
        ${CMAKE_CURRENT_BINARY_DIR}
        ${MOAB_INCLUDE_DIRS}
        ${VTK_INCLUDE_DIRS}
        )

    target_compile_definitions(${test_prog} PRIVATE "DATA_DIR=\"${PROJECT_SOURCE_DIR}/data\"")
    target_compile_definitions(${test_prog} PRIVATE "SCRATCH_DIR=\"${CMAKE_BINARY_DIR}/Testing/Temporary\"")
    target_compile_definitions(${test_prog} PRIVATE "WORKFLOWS_SOURCE_DIR=\"${PROJECT_SOURCE_DIR}/simulation-workflows\"")

    set(smtk_module_dir ${smtk_DIR}/${SMTK_PYTHON_MODULEDIR})
    if(NOT IS_ABSOLUTE ${smtk_module_dir})
      get_filename_component(smtk_module_dir
        ${PROJECT_BINARY_DIR}/${smtk_DIR}/${SMTK_PYTHON_MODULEDIR} ABSOLUTE)
    endif()


    foreach (test ${ut_SOURCES})
      get_filename_component(tname ${test} NAME_WE)
      add_test(NAME ${tname}
        COMMAND ${test_prog} ${tname} ${${tname}_EXTRA_ARGUMENTS}
        )
      set_tests_properties(${tname} PROPERTIES TIMEOUT 120
        ENVIRONMENT "PYTHONPATH=${PROJECT_BINARY_DIR}:${smtk_module_dir}:$ENV{PYTHONPATH}")
      if(ut_LABEL)
        set_tests_properties(${tname} PROPERTIES LABELS ${ut_LABEL})
      endif()
    endforeach(test)
  endif (ENABLE_TESTING)
endfunction(unit_tests)
