# All tests under this directory use files in data folder,
# which requires git-lfs
file(READ "${CMAKE_SOURCE_DIR}/data/ace3p-data" data)
if (NOT data STREQUAL "\n")
    message(WARNING
    "Testing data is not available. Use git-lfs in order to obtain the "
    "testing data.")
    return()
endif ()

set(CW16_PATH "" CACHE PATH "Location of data folder from SLAC 2018 Code Workshop.")

if (NOT DEFINED PYTHON_EXECUTABLE)
    message(WARNING "Skipping python and cw16 tests because PYTHON_EXECUTABLE not defined")
elseif (NOT SMTK_ENABLE_VTK_SUPPORT)
    message(WARNING "Skipping python and cw16 tests because SMTK_ENABLE_VTK_SUPPORT is off")
else ()
    add_subdirectory(python)

    if (NOT CW16_PATH)
        message(WARNING "Skipping cw16 tests because CW16_PATH not defined")
    elseif(NOT EXISTS ${CW16_PATH})
        message(WARNING "Skipping cw16 tests because CW16_PATH not found (${CW16_PATH})")
    else()
        add_subdirectory(cw16)
    endif()
endif()
