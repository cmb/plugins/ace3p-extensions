//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqNerscFileBrowserBehavior.h"

#include "smtk/newt/qtNewtFileBrowserWidget.h"
#include "smtk/newt/qtNewtInterface.h"

#include "pqCoreUtilities.h"

#include <QDebug>
#include <QStyle>
#include <QTimer>

//-----------------------------------------------------------------------------
pqNerscFileBrowserReaction::pqNerscFileBrowserReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

void pqNerscFileBrowserReaction::onTriggered()
{
  pqNerscFileBrowserBehavior* behavior = pqNerscFileBrowserBehavior::instance();
  behavior->showBrowser();
}

//-----------------------------------------------------------------------------
static pqNerscFileBrowserBehavior* g_instance = nullptr;

pqNerscFileBrowserBehavior::pqNerscFileBrowserBehavior(QObject* parent)
  : Superclass(parent)
  , m_browserWidget(new newt::qtNewtFileBrowserWidget)
{
}

pqNerscFileBrowserBehavior* pqNerscFileBrowserBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqNerscFileBrowserBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqNerscFileBrowserBehavior::~pqNerscFileBrowserBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

void pqNerscFileBrowserBehavior::showBrowser()
{
  m_browserWidget->show();
}
