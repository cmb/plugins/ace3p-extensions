
//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME pqNerscFileBrowserPanel - container for qtNewtFileBrowser
// .SECTION Description

#ifndef plugin_pqNerscFileBrowserPanel_h
#define plugin_pqNerscFileBrowserPanel_h

#include <QDockWidget>

class pqNerscFileBrowserPanel : public QDockWidget
{
  Q_OBJECT

public:
  pqNerscFileBrowserPanel(QWidget* parent);
  virtual ~pqNerscFileBrowserPanel();

private:
  class Internal;
  Internal* m_internal;
};

#endif
