//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_plugin_pqACE3PNewProjectBehavior_h
#define smtk_simulation_ace3p_plugin_pqACE3PNewProjectBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

class pqACE3PNewProjectBehaviorInternals;

/// \brief A reaction for creating a new ACE3P project.
class pqACE3PNewProjectReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqACE3PNewProjectReaction(QAction* parent);
  ~pqACE3PNewProjectReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PNewProjectReaction)
};

/// \brief Creates new ACE3P project.
class pqACE3PNewProjectBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PNewProjectBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PNewProjectBehavior() override;

  void newProject();

Q_SIGNALS:
  void projectCreated(smtk::project::ProjectPtr);

protected:
  pqACE3PNewProjectBehavior(QObject* parent = nullptr);

private:
  pqACE3PNewProjectBehaviorInternals* Internals;

  Q_DISABLE_COPY(pqACE3PNewProjectBehavior);
};

#endif
