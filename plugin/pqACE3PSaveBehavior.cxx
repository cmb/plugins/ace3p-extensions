//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PSaveBehavior.h"
#include "smtk/simulation/ace3p/operations/Write.h"

#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/Operation.h"
#include "smtk/project/Project.h"
#include "smtk/project/operators/Write.h"

// ParaView (client side)
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QString>
#include <QtGlobal>

#include <string>

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);
}

//-----------------------------------------------------------------------------
pqACE3PSaveReaction::pqACE3PSaveReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

void pqACE3PSaveReaction::onTriggered()
{
  pqACE3PSaveBehavior::instance()->saveProject();
}

//-----------------------------------------------------------------------------
bool pqACE3PSaveBehavior::saveProject()
{
  // Get current project
  auto project = smtk::simulation::ace3p::qtProjectRuntime::instance()->project();
  if (project == nullptr)
  {
    qWarning() << "Internal error - no active project.";
    return false;
  }

  // Access the active server to get the operation manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Instantiate the Write operator
  auto writeOp = opManager->create<smtk::simulation::ace3p::Write>();
  assert(writeOp != nullptr);

  writeOp->parameters()->associate(project);
  auto result = writeOp->operate();
  int outcome = result->findInt("outcome")->value();
  if (outcome != OP_SUCCEEDED)
  {
    std::string log = writeOp->log().convertToString();
    qWarning() << QString::fromStdString(log);

    QString msg = "There was an error saving the project."
                  " Check the Output Messages panel for details.";
    QMessageBox::warning(
      pqCoreUtilities::mainWidget(), "Failed to save project", msg, QMessageBox::Close);
    return false;
  }

  qInfo() << "Saved project";
  return true;
} // saveProject()

//-----------------------------------------------------------------------------
static pqACE3PSaveBehavior* g_instance = nullptr;

pqACE3PSaveBehavior::pqACE3PSaveBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqACE3PSaveBehavior* pqACE3PSaveBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqACE3PSaveBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqACE3PSaveBehavior::~pqACE3PSaveBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
