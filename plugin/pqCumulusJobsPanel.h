//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME pqCumulusJobsPanel - display jobs on remote system
// .SECTION Description

#ifndef __CmbJobsPanels_h
#define __CmbJobsPanels_h

#include <QDockWidget>

class pqCumulusJobsPanel : public QDockWidget
{
  Q_OBJECT

public:
  pqCumulusJobsPanel(QWidget* parent);
  virtual ~pqCumulusJobsPanel();

Q_SIGNALS:
  void loadSimulationResults(const QString& path);
  void resultDownloaded(const QString& path);

public Q_SLOTS:

protected Q_SLOTS:
  void infoSlot(const QString& msg);
  void authenticateHPC();
  void requestSimulationResults();

private:
  class pqCumulusJobsPanelInternal;
  pqCumulusJobsPanelInternal* Internal;
};

#endif // __CmbJobsPanels_h
