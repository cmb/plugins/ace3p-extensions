//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "pqACE3PMenu.h"

#include "pqACE3PCloseBehavior.h"
#include "pqACE3PExportBehavior.h"
#include "pqACE3PNewProjectBehavior.h"
#include "pqACE3PNewStageBehavior.h"
#include "pqACE3POpenBehavior.h"
#include "pqACE3PProjectLoader.h"
#include "pqACE3PRecentProjectsMenu.h"
#include "pqACE3PRemoteParaViewBehavior.h"
#include "pqACE3PSaveBehavior.h"
#include "pqNerscFileBrowserBehavior.h"
#include "pqNerscLoginBehavior.h"

#include "smtk/newt/qtNewtInterface.h"
#include "smtk/simulation/ace3p/Project.h"
#include "smtk/simulation/ace3p/qt/qtMenuSeparator.h"
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

// ParaView includes
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"
#include "pqServerManagerModel.h"
#include "vtkSMIntVectorProperty.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

// Qt includes
#include <QAction>
#include <QDebug>
#include <QList>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QString>
#include <QTimer>

pqACE3PMenu::pqACE3PMenu(QObject* parent)
  : Superclass(parent)
{
  this->startup();
}

//-----------------------------------------------------------------------------
pqACE3PMenu::~pqACE3PMenu()
{
  this->shutdown();
}

//-----------------------------------------------------------------------------
bool pqACE3PMenu::startup()
{
  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "cannot initialize ACE3P menu because pqCore is not found";
    return false;
  }

  // Access/create the singleton instances of our behaviors.
  auto openProjectBehavior = pqACE3POpenBehavior::instance(this);
  auto newProjectBehavior = pqACE3PNewProjectBehavior::instance(this);
  auto newStageBehavior = pqACE3PNewProjectBehavior::instance(this);
  auto saveProjectBehavior = pqACE3PSaveBehavior::instance(this);
  auto exportProjectBehavior = pqACE3PExportBehavior::instance(this);
  auto nerscLoginBehavior = pqNerscLoginBehavior::instance(this);
  auto closeProjectBehavior = pqACE3PCloseBehavior::instance(this);

  QObject::connect(
    newProjectBehavior,
    &pqACE3PNewProjectBehavior::projectCreated,
    this,
    &pqACE3PMenu::onProjectOpened);

  // Initialize Open Project action
  m_openProjectAction = new QAction(tr("Open Project..."), this);
  auto openProjectReaction = new pqACE3POpenReaction(m_openProjectAction);
  auto startIcon = QIcon(":/icons/toolbar/openProject.svg");
  m_openProjectAction->setIcon(startIcon);
  m_openProjectAction->setIconVisibleInMenu(true);

  // Initialize Recent Projects menu
  m_recentProjectsAction = new QAction(tr("Recent Projects"), this);
  QMenu* recentProjectsMenu = new QMenu();
  m_recentProjectsAction->setMenu(recentProjectsMenu);
  m_recentProjectsMenu = new pqACE3PRecentProjectsMenu(recentProjectsMenu, recentProjectsMenu);

  // Initialize New Project action
  m_newProjectAction = new QAction(tr("New Project..."), this);
  auto newProjectReaction = new pqACE3PNewProjectReaction(m_newProjectAction);
  auto newIcon = QIcon(":/icons/toolbar/newProject.svg");
  m_newProjectAction->setIcon(newIcon);
  m_newProjectAction->setIconVisibleInMenu(true);

  // Initialize New Stage action
  m_newStageAction = new QAction(tr("Add Stage..."), this);
  auto newStageReaction = new pqACE3PNewStageReaction(m_newStageAction);
  //auto newIcon = QIcon(":/icons/toolbar/newProject.svg");
  m_newStageAction->setIcon(newIcon);
  m_newStageAction->setIconVisibleInMenu(true);

  // Initialize Save Project action
  m_saveProjectAction = new QAction(tr("Save Project"), this);
  auto saveProjectReaction = new pqACE3PSaveReaction(m_saveProjectAction);
  auto saveIcon = QIcon(":/icons/toolbar/saveProject.svg");
  m_saveProjectAction->setIcon(saveIcon);
  m_saveProjectAction->setIconVisibleInMenu(true);

  // Initialize Close Project action
  m_closeProjectAction = new QAction(tr("Close Project"), this);
  auto closeProjectReaction = new pqACE3PCloseReaction(m_closeProjectAction);
  auto closeIcon = QIcon(":/icons/toolbar/closeProject.svg");
  m_closeProjectAction->setIcon(closeIcon);
  m_closeProjectAction->setIconVisibleInMenu(true);

  // Initialize NERSC login action (hidden until finalize)
  m_nerscLoginAction = new QAction(tr("Login to NERSC"), this);
  auto nerscLoginReaction = new pqNerscLoginReaction(m_nerscLoginAction);
  auto signinIcon = QIcon(":/icons/toolbar/sign-in-alt.svg");
  m_nerscLoginAction->setIcon(signinIcon);
  m_nerscLoginAction->setIconVisibleInMenu(true);
  m_nerscLoginAction->setVisible(false);

  // Initialize NERSC Submit (Export Project) action (hidden until finalize)
  m_nerscSubmitAction = new QAction(tr("Submit Analysis Job..."), this);
  auto exportProjectReaction = new pqACE3PExportReaction(m_nerscSubmitAction);
  auto exportIcon = QIcon(":/icons/toolbar/exportProject.svg");
  m_nerscSubmitAction->setIcon(exportIcon);
  m_nerscSubmitAction->setIconVisibleInMenu(true);
  m_nerscSubmitAction->setVisible(false);

  // Defer remote paraview menu until finalize()

  // Initialize Amazon EC2 List Clusters action (hidden until finalize)
  auto listClustersIcon = QIcon(":/icons/toolbar/pcluster.png");
  m_amazonListClustersAction = new QAction(listClustersIcon, "List Clusters", this);
  m_amazonListClustersAction->setVisible(false);

  // Initialize Submit Amazon EC2 Analysis Job action (hidden until finalize)
  m_amazonSubmitAction = new QAction(exportIcon, "Submit Analysis Job...", this);
  m_amazonSubmitAction->setVisible(false);

  QObject::connect(
    closeProjectBehavior,
    &pqACE3PCloseBehavior::projectClosed,
    this,
    &pqACE3PMenu::onProjectClosed);

  // For now, presume that there is no project loaded at startup
  this->onProjectClosed();

  auto projectLoader = pqACE3PProjectLoader::instance();
  QObject::connect(
    projectLoader, &pqACE3PProjectLoader::projectOpened, this, &pqACE3PMenu::onProjectOpened);

  // Finish menu after builtin server is connected, so that we can get settings.
  pqServer* server = pqActiveObjects::instance().activeServer();
  if (server == nullptr)
  {
    pqServerManagerModel* model = pqApplicationCore::instance()->getServerManagerModel();
    m_serverConnection =
      QObject::connect(model, &pqServerManagerModel::serverAdded, this, &pqACE3PMenu::finalizeMenu);
  }
  else
  {
    this->finalizeMenu(server);
  }

  return true;
}

void pqACE3PMenu::shutdown() {}

//-----------------------------------------------------------------------------
void pqACE3PMenu::onProjectOpened(smtk::project::ProjectPtr project)
{
  m_openProjectAction->setEnabled(false);
  m_recentProjectsAction->setEnabled(false);
  m_newProjectAction->setEnabled(false);
  m_newStageAction->setEnabled(true);
  // Note: m_saveProjectAction is set according to project's modified state
  m_closeProjectAction->setEnabled(true);
  m_nerscSubmitAction->setEnabled(true);
  m_amazonSubmitAction->setEnabled(true);

  if (!m_saveMenuConfigured)
  {
    m_saveMenuConfigured = true; // only do this once
    m_saveProjectAction->setEnabled(false);

    // Get ACE3P menu and enable the save-project item based on project state
    QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());
    if (mainWindow == nullptr)
    {
      qWarning() << __FILE__ << __LINE__ << "Internal Error main window null.";
      return;
    }

    QList<QAction*> menuBarActions = mainWindow->menuBar()->actions();
    QMenu* menu = nullptr;
    Q_FOREACH (QAction* existingMenuAction, menuBarActions)
    {
      QString menuName = existingMenuAction->text();
      menuName.remove('&');
      if (menuName == "ACE3P")
      {
        menu = existingMenuAction->menu();
        break;
      }
    }

    if (menu == nullptr)
    {
      qWarning() << __FILE__ << __LINE__ << "Internal Error ACE3P menu not found.";
      return;
    }

    // Update the project-save item each time the menu is activated
    QObject::connect(menu, &QMenu::aboutToShow, [this]() {
      const auto project = smtk::simulation::ace3p::qtProjectRuntime::instance()->project();
      bool modified = project && !project->clean();
      this->m_saveProjectAction->setEnabled(modified);
    });
  }
}

//-----------------------------------------------------------------------------
void pqACE3PMenu::onProjectClosed()
{
  m_openProjectAction->setEnabled(true);
  m_recentProjectsAction->setEnabled(true);
  m_newProjectAction->setEnabled(true);
  m_newStageAction->setEnabled(false);
  m_closeProjectAction->setEnabled(false);
  m_saveProjectAction->setEnabled(false);
  m_saveProjectAction->setEnabled(false);
  m_nerscSubmitAction->setEnabled(false);
  m_amazonSubmitAction->setEnabled(false);
}

//-----------------------------------------------------------------------------
void pqACE3PMenu::finalizeMenu(pqServer* server)
{
  // Disconnect from sender so that this is only called once.
  if (m_serverConnection)
  {
    QObject::disconnect(m_serverConnection);
  }

  // Make sure that the main window has been created.
  QMainWindow* mainWindow = qobject_cast<QMainWindow*>(pqCoreUtilities::mainWidget());
  if (mainWindow == nullptr)
  {
    QTimer::singleShot(10, [this, server]() { this->finalizeMenu(server); });
    return;
  }

  // Search for the ACE3P menu
  QList<QAction*> menuBarActions = mainWindow->menuBar()->actions();
  QMenu* menu = nullptr;
  Q_FOREACH (QAction* existingMenuAction, menuBarActions)
  {
    QString menuName = existingMenuAction->text();
    menuName.remove('&');
    if (menuName == "ACE3P")
    {
      menu = existingMenuAction->menu();
      break;
    }
  }

  if (menu == nullptr)
  {
    QTimer::singleShot(10, [this, server]() { this->finalizeMenu(server); });
    return;
  }

  // Check settings for NERSC and Amazon EC2 support
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "ACE3PSettings");
  if (!proxy)
  {
    qWarning() << "Internal Error: Settings proxy for ACE3P not found."
               << " Cannot initialize NERSC or Amazon EC2 menu items.";
    return;
  }

  bool enableNersc = false;
  bool enableAmazon = false;

  vtkSMProperty* nerscProp = proxy->GetProperty("NERSCInterface");
  auto* intNerscProp = vtkSMIntVectorProperty::SafeDownCast(nerscProp);
  enableNersc = static_cast<bool>(intNerscProp->GetElement(0));

#if 0
  vtkSMProperty* amazonProp = proxy->GetProperty("EC2Interface");
  auto* intAmazonProp = vtkSMIntVectorProperty::SafeDownCast(amazonProp);
  enableAmazon = static_cast<bool>(intAmazonProp->GetElement(0));
  if (enableAmazon)
  {
    // Amazon interface requires private key file
    vtkSMProperty* keyProp = proxy->GetProperty("AmazonPrivateKeyFile");
    auto* strKeyProp = vtkSMStringVectorProperty::SafeDownCast(keyProp);
    QString keyFileName = strKeyProp->GetElement(0);
    if (keyFileName.isEmpty())
    {
      qWarning() << "Amazon EC2 selected in settings but no key file provided."
        << " Cannot initialize Amazon EC2 menu.";
      enableAmazon = false;
    }
    else
    {
      // Check that key file exists.
      QFileInfo keyFileInfo(keyFileName);
      if (!keyFileInfo.exists())
      {
      qWarning() << "Amazon EC2 private key file listed in settings not found: "
        << keyFileName << " Cannot initialize Amazon EC2 menu.";
      enableAmazon = false;

      }
    }
  }
#endif

  // Insert separators before NERSC and AWS
  QList<QAction*> actions = menu->actions();
  Q_FOREACH (QAction* action, actions)

  {
    if (enableNersc && (action == m_nerscLoginAction))
    {
      QIcon nerscIcon(":/icons/toolbar/nersc.svg");
      qtMenuSeparator* sep = new qtMenuSeparator("NERSC", nerscIcon);
      menu->insertAction(action, sep);

      m_nerscLoginAction->setVisible(true);
      m_nerscSubmitAction->setVisible(true);
    }
    else if (action == m_amazonListClustersAction)
    {
      if (enableNersc)
      {
        m_nerscParaViewMenu = new pqACE3PRemoteParaViewMenu;
        QAction* pvAction = menu->insertMenu(m_amazonListClustersAction, m_nerscParaViewMenu);
        QIcon pvIcon(":/icons/pvIcon.svg");
        pvAction->setIcon(pvIcon);
      }

      if (enableAmazon)
      {
        QIcon amazonIcon(":/icons/toolbar/amazon-ec2.svg");
        qtMenuSeparator* sep = new qtMenuSeparator("Amazon EC2", amazonIcon);
        menu->insertAction(action, sep);

        m_amazonListClustersAction->setVisible(true);
        m_amazonSubmitAction->setVisible(true);
      }
    } // if (m_amazonListClustersAction)
  }   // Q_FOREACH (action)
}
