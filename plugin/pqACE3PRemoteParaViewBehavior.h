//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PRemoteParaViewBehavior_h
#define pqACE3PRemoteParaViewBehavior_h

#include <QMenu>
#include <QObject>

#include <QMap>
#include <QPointer>
#include <QString>

class qtMessageDialog;

class pqServer;
class QAction;
class QWidget;

/** \brief Menu for launching PVServer on remote machines
 *
 *  Hard-coded for NERSC machines: Cori now, Perlmutter later
 */
class pqACE3PRemoteParaViewMenu : public QMenu
{
  Q_OBJECT

public:
  /**
  * Assigns the menu that will display the list of remote machines
  */
  pqACE3PRemoteParaViewMenu(QWidget* parent = nullptr);
  ~pqACE3PRemoteParaViewMenu() override = default;

private Q_SLOTS:
  void buildMenu();
  void onTriggered();

private:
  Q_DISABLE_COPY(pqACE3PRemoteParaViewMenu);
};

/** \brief Add behavior for starting remote PVServer
  */
class pqACE3PRemoteParaViewBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PRemoteParaViewBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PRemoteParaViewBehavior() override;

  pqServer* remoteServer(const QString& machine) const;
  bool startRemoteServer(const QString& machine);

Q_SIGNALS:
  void serverAdded(pqServer* server);

protected:
  pqACE3PRemoteParaViewBehavior(QObject* parent = nullptr);

  // Store <machine, pqServer> dictionary
  QMap<QString, pqServer*> m_serverMap;

  qtMessageDialog* m_messageDialog = nullptr;

  // Used to shut off disconnect message dialog after the first occurrence.
  bool m_showDisconnectDialog = true;

private Q_SLOTS:
  void serverTimeoutIn5Mins();
  void serverTimeoutIn1Min();
  void serverTimeout();

private:
  Q_DISABLE_COPY(pqACE3PRemoteParaViewBehavior);
};

#endif // pqACE3PRemoteParaViewBehavior_h
