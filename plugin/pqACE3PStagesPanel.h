//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME pqACE3PStagesPanel - display jobs on remote system
// .SECTION Description

#ifndef plugin_pqACE3PStagesPanels_h
#define plugin_pqACE3PStagesPanels_h

#include "smtk/extension/qt/qtUIManager.h"

#include <QDockWidget>

class pqSMTKWrapper;
class pqServer;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class qtStagesWidget;
}
} // namespace simulation
} // namespace smtk

class QTableView;

class pqACE3PStagesPanel : public QDockWidget
{
  Q_OBJECT

public:
  pqACE3PStagesPanel(QWidget* parent);
  virtual ~pqACE3PStagesPanel();

Q_SIGNALS:
  void projectUpdated(smtk::project::ProjectPtr);
  void requestNERSCNavigateTo(const QString& dir);
  void updateCurrentStage(int currentStage);

protected Q_SLOTS:
  void infoSlot(const QString& msg);
  void deleteStage(int index);

  /** \brief Triggers the Jobs Panel filtering based on the active Stage. */
  void triggerSelectStage(smtk::project::ProjectPtr project);

  virtual void sourceRemoved(pqSMTKWrapper* mgr, pqServer* server);

protected:
  smtk::simulation::ace3p::qtStagesWidget* m_stageWidget;
};

#endif // __ACE3PAnalysisPanels_h
