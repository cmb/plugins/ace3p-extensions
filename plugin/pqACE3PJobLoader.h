//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PJobLoader_h
#define pqACE3PJobLoader_h

#include <QObject>

class QDir;
class pqFileDialogModel;
class pqPipelineSource;
class pqServer;
class pqView;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class qtModeSelectDialog;
} // namespace ace3p
} // namespace simulation
} // namespace smtk

class qtProgressDialog;

class pqACE3PJobLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  // @brief return an instance of the loader
  static pqACE3PJobLoader* instance(QObject* parent = nullptr);

  // @brief destructor
  ~pqACE3PJobLoader();

  // @brief convenience function for destroying a pipeline
  // @note reimplemented from SLACTools
  static void destroyPipelineSourceAndConsumers(pqPipelineSource* source);
public Q_SLOTS:
  // @brief handle load-job request
  void onRequestLoadJob(const QString& jobId, bool remote);

protected Q_SLOTS:
  // @brief start process to setup pipeline
  void onModelFilesSelected(const QStringList& modeFiles);

protected:
  // @brief open a new view and load the results in the JobDir
  void setupPipeline(const QStringList& modeFiles);

  // @brief Initializes particles reader (or returns null if not used)
  pqPipelineSource* setupParticlesReader(pqServer* server);

  // @brief set coloring and other viz options
  void initVizOptions(pqPipelineSource* meshReader, pqView* meshView);

  // @brief open a dialog for selecting mode files for given project
  void openModeSelectDialog(const QString& jobId, pqServer* pqServer);

  // @brief protected constructor
  pqACE3PJobLoader(QObject* parent = nullptr);

  // @brief find a pqView or create one if needed
  pqView* findView(const pqPipelineSource* source);

  // @brief find a pipeline source. reimplemented from pqSLACManager
  pqPipelineSource* findPipelineSource(const char* SMName);

  // @brief get the mode files from a job
  QStringList getModeFileNames(const QString& directory, pqFileDialogModel& fileModel);

  // @brief get the particles files from a job (applies to Track3P only)
  void getParticlesFileNames(pqFileDialogModel& fileModel, QStringList& particleFiles) const;

  // holds on to the popup dialog for selecting mode files
  smtk::simulation::ace3p::qtModeSelectDialog* m_dialog;

  // Current job data
  QString m_meshFileLocation;
  QString m_modesDirectory;
  QString m_particlesDirectory;
  QString m_fieldName;

  qtProgressDialog* m_progressDialog = nullptr;

private:
  Q_DISABLE_COPY(pqACE3PJobLoader);
};
#endif
