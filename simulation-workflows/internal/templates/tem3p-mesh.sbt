<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>TEM3P-Eigen</Cat>
    <Cat>TEM3P-Elastic</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="MeshDump" Label="Mesh Dump" BaseType="" Version="0">
      <ItemDefinitions>
        <Group Name="DeformedMeshFiles" Label="Write Deformed Mesh Files" Version="0">
          <ItemDefinitions>
            <Void Name="WriteDeformedMesh" Label="Write Deformed Mesh" Optional="true" IsEnabledByDefault="true" Version="0">
              <BriefDescription>Write the deformed mesh</BriefDescription>
              <Categories>
                <Cat>TEM3P-Eigen</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
            </Void>
            <!-- Note WriteDeformedEMMesh uses NERSC Directory in ACE3P.sbt-->
            <Void Name="WriteDeformedEMMesh" Label="Write Deformed EM Mesh" Optional="true" IsEnabledByDefault="true" Version="0">
              <Categories>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
            </Void>
            <Double Name="MeshDeformScale" Label="Mesh Deform Scale" Version="0">
              <BriefDescription>Deformation scale factor</BriefDescription>
              <Categories>
                <Cat>TEM3P-Eigen</Cat>
                <Cat>TEM3P-Elastic</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Void Name="WriteStressStrain" Label="Write Stress/Strain Mode Files" Optional="true" IsEnabledByDefault="true" Version="0">
          <BriefDescription>Write the stress/strain .mod files</BriefDescription>
          <Categories>
            <Cat>TEM3P-Eigen</Cat>
            <Cat>TEM3P-Elastic</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
</SMTK_AttributeResource>