#!/usr/bin/env bash

# Custom script to run convert_rfpost.py script on john's main dev mahine (turtleland4)


# Get script directory, per http://stackoverflow.com/questions/59895
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $TARGET == /* ]]; then
    #echo "SOURCE '$SOURCE' is an absolute symlink to '$TARGET'"
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    #echo "SOURCE '$SOURCE' is a relative symlink to '$TARGET' (relative to '$DIR')"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
#echo "SOURCE is '$SOURCE'"
RDIR="$( dirname "$SOURCE" )"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
# if [ "$DIR" != "$RDIR" ]; then
#   echo "DIR '$RDIR' resolves to '$DIR'"
# fi
echo "script DIR is '$DIR'"


PYTHONPATH=/home/john/projects/slac/build/dev-smtk/lib/python3.7/site-packages:/home/john/projects/slac/build/dev-superbuild/install/lib/python3.7/site-packages \
  /home/john/projects/slac/build/dev-superbuild/install/bin/python3.7m \
  ${DIR}/convert_rfpost.py \
  ${DIR}/sample.rfpost
