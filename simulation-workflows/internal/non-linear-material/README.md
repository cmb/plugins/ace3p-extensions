# Nonlinear material files

The files in this folder were downloaded from SLAC using

    wget -r -erobots=off -np -nH -R index.html https://www.slac.stanford.edu/~liling/non-linear-material/

Some files starting index.html also get download - these were deleted manually

A materials spreadsheet was provided by SLAC, and uploaded to Kitware's Google Drive at:

    https://docs.google.com/spreadsheets/d/1XclXerRWk-fKbqoFW2H-DEfXTyAC7NjEIt5U99o0AVw/edit?usp=sharing
